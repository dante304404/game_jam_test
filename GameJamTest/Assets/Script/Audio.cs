﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour
{

        private static Audio _instance;
        public AudioSource MusicSource;

        void Awake()
        {
            //if we don't have an [_instance] set yet
            if (!_instance)
                _instance = this;
            //otherwise, if we do, kill this thing
            else
                Destroy(this.gameObject);


            DontDestroyOnLoad(this.gameObject);
        }

        public void PlayMusic()
        {
        if (Input.GetKeyDown(KeyCode.Space))
            MusicSource.Play();
            
            
        }
}
