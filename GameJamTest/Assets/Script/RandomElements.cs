﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomElements : MonoBehaviour 
{
	public List<GameObject> lineList;
	public List<GameObject> objectList;
	public float timeSpawn;
	float timer = 0f;

	void Update()
	{

		timer += Time.deltaTime;

		if (timer > timeSpawn) 
		{
			var randomObject = Random.Range (0, objectList.Count);
			var randomList = Random.Range (0, lineList.Count);
			Instantiate (objectList [randomObject],lineList[randomList].transform.position,lineList[randomList].transform.rotation);
			timer = 0.0f;
		}


	}

}
