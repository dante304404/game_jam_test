﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckObject : MonoBehaviour 
{
	public KeyCode KeyToPress;
	public GameObject hero;
	public GameObject Enemy;
	public Slider mana;
	public Slider life;
	public Text goldValue;

	void Update()
	{
		if (Input.GetKey (KeyToPress)) 
		{
			GetComponentInChildren<Animator> ().SetBool ("Press",true);
		}
		if (Input.GetKeyUp (KeyToPress)) 
		{
			GetComponentInChildren<Animator> ().SetBool ("Press",false);
		}
	}

	void OnTriggerStay2D(Collider2D other)
	{
		Press (other);
	}


	void Press(Collider2D other)
	{
		if (other.gameObject.tag == "Sword" && Input.GetKey(KeyToPress)) 
		{
			Debug.Log ("Collect Sword");
			hero.GetComponentInChildren<Animator> ().SetBool ("Attack", true);
			Destroy (other.gameObject);
			Enemy.GetComponent<Enemy> ().GetDamage ();

		}
		if (other.gameObject.tag == "Life" && Input.GetKey(KeyToPress)) 
		{
			Debug.Log ("Collect Life");
			if (life.value < 100) 
			{
				life.value = life.value + 10.0f;
			}
			Destroy (other.gameObject);
		}
		if (other.gameObject.tag == "Diamond" && Input.GetKey(KeyToPress)) 
		{
			Debug.Log ("Collect Diamond");
			var gold = System.Convert.ToInt32 (goldValue.text) + 100;
			goldValue.text = System.Convert.ToString (gold);
			Destroy (other.gameObject);
		}
		if (other.gameObject.tag == "Book" && Input.GetKey (KeyToPress)) 
		{
			Debug.Log ("Collect Book");
			if (mana.value < 100) 
			{
				mana.value = mana.value + 10.0f;
			}
		
			Destroy (other.gameObject);
		} 
	}
}
