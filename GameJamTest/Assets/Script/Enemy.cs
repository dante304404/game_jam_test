﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour 
{
	public float life;
	public GameObject randomLines;
	private float eternalLife;


	void Start()
	{
		eternalLife = life;
	}

	public void GetDamage()
	{
		life--;
		if (life > 0) 
		{
			GetComponentInChildren<Animator> ().SetBool ("Damage", true);
		}
		if (life <= 0) 
		{
			Death ();
		}
	}

	public void Death()
	{
		GetComponentInChildren<Animator> ().SetBool ("Death", true);
		if (randomLines.GetComponent<RandomElements> ().timeSpawn > 0.3f) 
		{
			randomLines.GetComponent<RandomElements> ().timeSpawn = randomLines.GetComponent<RandomElements> ().timeSpawn - 0.1f;
		}
	
		life = eternalLife;
	}

}
