﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spell : MonoBehaviour 
{
	public GameObject hero;
	public GameObject enemy;
	public Slider mana;

	void Update()
	{
		if (Input.GetKey(KeyCode.Space) && mana.value == 100)
		{
			hero.GetComponentInChildren<Animator> ().SetBool ("Magic", true);
			mana.value = 0;
			enemy.GetComponent<Enemy> ().Death ();
		}
	}

}
