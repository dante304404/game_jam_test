﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOffAnimation : MonoBehaviour {

	void TurnOffAttack()
	{
		GetComponent<Animator> ().SetBool ("Attack", false);

	}

	void TurnOffMagic()
	{
		GetComponent<Animator> ().SetBool ("Magic", false);

	}

	void TurnOffDamage()
	{
		GetComponent<Animator> ().SetBool ("Damage", false);

	}

	void RaiseAgain()
	{
		GetComponent<Animator> ().SetBool ("Raise", true);
		GetComponent<Animator> ().SetBool ("Death", false);
	}

	void EndRaise()
	{
		GetComponent<Animator> ().SetBool ("Raise", false);
	}
}
