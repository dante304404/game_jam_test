﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestroyObject : MonoBehaviour 
{
	public Slider life;
	public GameObject hero;
	public GameObject enemy;

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Sword") 
		{
			enemy.GetComponentInChildren<Animator> ().SetBool ("Attack", true);
			hero.GetComponentInChildren<Animator> ().SetBool ("Damage", true);
			life.value = life.value - 10.0f;
		}
		Destroy (col.gameObject);
	}
}
