﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ToMenu : MonoBehaviour {

	void Update()
	{
		if (Input.GetKey (KeyCode.Escape))
		{
			ExitToMenu ();
		}
	}

	void ExitToMenu()
	{
		SceneManager.LoadScene("Menu");

	}
}
