﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class level1menu : MonoBehaviour 
{
	public Slider life;


	void Update()
	{
		if (Input.GetKey(KeyCode.Escape))
		{
			ExitToMenu();
		}
		if (life.value <= 0) 
		{
			Death ();
		}
	}

	public void Death()
	{
		SceneManager.LoadScene ("GameOver");
	}

	void ExitToMenu()
	{
		SceneManager.LoadScene("Menu");

	}
}
